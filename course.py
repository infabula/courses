class Course(object):
    """
    A trainingscourse.
    """

    def __init__(self, name):
        """Constructor."""
        self.name = name
        self.students = []
