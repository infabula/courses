class Person(object):
    """Person."""

    def __init__(self, name='', birth_date=None):
        """Constructor."""
        self.name = name
        self.birth_date = birth_date
