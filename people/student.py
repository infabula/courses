from .person import Person

class Student(Person):
    '''Student.'''
    MOTIVATION = ['lazy', 'highly']

    def __init__(self,
                 motivation=MOTIVATION[0],
                 *args, **kwargs):
        '''Constructor.'''
        self.motivation = motivation
        super().__init__(*args, **kwargs)
