from .person import Person

class Trainer(Person):
    '''Trainer.'''
    nb_of_created_instances = 0
    nb_of_active_trainers = 0

    trainer_types = ['lazy', 'agile', 'sharp']

    def __init__(self, specialisation, name, birth_date):
        '''Constructor.'''
        self.specialisation = specialisation
        Trainer.nb_of_created_instances += 1
        Trainer.nb_of_active_trainers += 1
        super().__init__(name, birth_date)

    def __del__(self):
        '''Carbage '''
        print("{} is being garbage collected".format(self.name))
        Trainer.nb_of_active_trainers -= 1
