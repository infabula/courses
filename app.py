import sys
import datetime
import json

from people.trainer import Trainer
from people.student import Student
import course


python_course = course.Course('Python')
python_trainer = Trainer(
    specialisation="Programming",
    name="Mr. T",
    birth_date=datetime.date(1965, 1, 1)
)

student = Student()
