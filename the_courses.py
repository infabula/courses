import datetime

def print_courses(courses):
    """
    Prints the course names in the courses list.

    Returns nothing, but pollutes the stout.
    """
    # voor alle elementen in de lijst:
    #     van dat element print de naam
    for course in courses:
        # course is a dict
        print(course['name'])


def add_student(course, student, grade):
    '''
    Add the student to the students list of the course.

    And add the grade to the grade list.
    '''

def mean_grade(grade_list):
    ''' Return the mean value of all the grades.'''
    if len(grade_list) == 0:
        raise ZereDivsionError("list is empty")
    else:
        total = 0.0
        for grade in grade_list:
            total += grade
        mean = total / len(grade_list)
        return mean

course_list = []

course_1 = {
    'name': 'Introduction Python',
    'students': [],
    'grades': [],
}

course_2 = {
    'name': 'Advanced Python',
    'students': [],
    'grades': [],
}

course_list.append(course_1)  # add the course
course_list.append(course_2)  # add the course

student_1 = {
    'forename': 'Jan-Willem',
    'surname': 'De Heij',
    'date_of_birth': datetime.date(1970, 1, 21)
}

student_2 = {
    'forename': 'Maaikel',
    'surname': 'Groenendijk',
    'date_of_birth': datetime.date(1985, 7, 18)
}

student_3 = {
    'forename': 'Stephan',
    'surname': 'Dries',
    'date_of_birth': datetime.date(1985, 2, 1)
}

if __name__ == '__main__':
    print_courses(course_list)
    try:
        grade_list = [5.5, 4.7, 8.9, 10.0]
        mean = mean_grade(grade_list)
        print("The mean value is {}".format(mean))
    except ZeroDivisionError as e:
        print(e)
    except Exceptions as e:
        print(e)

    #add_student(course_1, student_1, 8)
